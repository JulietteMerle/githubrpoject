import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

//we can destructure inside the arrow function
const UserItem = ({user: { login, avatar_url, html_url}}) => { 
    return (
      <div className="card text-center">
        <img src={avatar_url}
         alt='' 
         className='round-img'
         style={{width: '60px'}} />
         <h3>{login}</h3>
         <div>
             <Link to={`/user/${login}`} className="btn btn-dark btn-sm my-1">More</Link>
         </div>
      </div>
    )
};

//we pass props in the arrow function
UserItem.propTypes = {
    user: PropTypes.object.isRequired,
}

export default UserItem
