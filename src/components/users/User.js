import React, { Fragment, useEffect, useContext } from 'react';
import Spinner from '../layout/Spinner';
import Repos from '../repos/Repos';
import { Link } from 'react-router-dom';
import GithubContext from '../../context/github/githubContext';

const User =({ match})=>  {
    const githubContext = useContext(GithubContext);
    const { getUser, loading, user, repos, getUserRepos } = githubContext;

    useEffect(() => {
        getUser(match.params.login);
        getUserRepos(match.params.login);
        //eslint-disable-next-line 
    }, []);

    const {
         name,
         company,
         avatar_url,
         location,
         bio,
         blog,
         login,
         html_url,
         followers, 
         following,
         public_repos,
         public_gists,
         hireable
        } = user;

        //bring the spinner to the user page
        if(loading) return <Spinner />

        return (
            <Fragment> 
            {/* we search, clisue on a user and go back to search ==> search is still there thanks to Link */}
                <Link to='/' className='btn btn-light'>
                    Back to search
                </Link>
                {/* Hireable ==> if the user is hereable==> check green icon otherwise circle red icon */}
                Hireable: {' '}
                {hireable ? (<i className="fas fa-check text-success" /> ): (<i className="fas fa-times-circle text-danger" />)}
                {/* grid-2 = the first div will be on the left and the second on the right */}
                <div className="card grid-2">
                    <div className="all-center">
                        <img src={avatar_url} className="round-img" alt="" style={{width: '150px'}}/>
                        <h1>{name}</h1>
                        <p>Location: {location}</p>
                    </div>
                    <div>
                    {/* If the user has a bio, show it if not.. don't show anything */}
                        {bio && <Fragment>
                            <h3>Bio</h3>
                            <p>{bio}</p>
                        </Fragment>}
                        <a href={html_url} className="btn btn-dark my-1">Visit Github Profile</a>
                        <ul>
                            <li>
                                {login && <Fragment>
                                    <strong>Username: </strong> {login}
                                </Fragment>}
                            </li>
                            <li>
                                {company && <Fragment>
                                    <strong>Company: </strong> {company}
                                </Fragment>}
                            </li>
                            <li>
                                {blog && <Fragment>
                                    <strong>Website: </strong> {blog}
                                </Fragment>}
                            </li>
                        </ul>
                    </div>
                </div>

                <div className="card text-center">
                    <div className="badge badge-primary">Followers: {followers}</div>
                    <div className="badge badge-success">Following: {following}</div>
                    <div className="badge badge-light">Public repos: {public_repos}</div>
                    <div className="badge badge-dark">Public gists: {public_gists}</div>
                </div>
                <Repos repos={repos} />
            </Fragment>   

        )
}

export default User;
