import React, { useState, useContext } from 'react';
import GithubContext from '../../context/github/githubContext';
import AlertContext from '../../context/alert/alertContext';

const Search = () => {
  //Need to initialise ontext 
  const githubContext = useContext(GithubContext);
  const alertContext = useContext(AlertContext);

  //create a set text here
   const [text, setText] = useState('');

    const onChange = event => setText(event.target.value);

    const onSubmit = (e) => {
      e.preventDefault();
      if(text === '') {
        alertContext.setAlert('Please enter something', 'light')
      } else {
        githubContext.searchUsers(text);
        setText('');
      }
    };

 
  return (
      <div>
        <form onSubmit={onSubmit} className="form">
            <input 
            type="text" 
            name="text" 
            placeholder="Search Users..." 
            value={text}
            onChange={onChange}
            />
            <input type="submit" value="Search" className="btn btn-dark btn-block"/>
        </form>
        {/* need to know if the user's array >0 if yes ==> show the clear button*/}
        {githubContext.users.length > 0 && (<button className="btn btn-light btn-block"
           onClick={githubContext.clearUsers}>Clear</button>
        )}
      </div>
    )
  }

export default Search
