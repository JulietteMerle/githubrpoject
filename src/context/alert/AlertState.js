//our actions instead of App.js
import React, { useReducer } from 'react';
import AlertContext from './alertContext';
import AlertReducer from './alertReducer';
import { 
    SET_ALERT, REMOVE_ALERT
} from '../types';

//create initial state
const AlertState = props => {
    //there only one thing to initialize ==> can only put null
    const initialState = null;

    const [state, dispatch] = useReducer(AlertReducer, initialState);

    //set alert
    const setAlert = (msg, type) => {
        dispatch({
            type: SET_ALERT,
            payload:  { msg, type }
        })
        setTimeout(() =>  dispatch({ type: REMOVE_ALERT }), 3000);
    };

      return <AlertContext.Provider
        value={{
            alert: state,
            setAlert
            }}>
            {props.children}
    </AlertContext.Provider>
}

export default AlertState;

