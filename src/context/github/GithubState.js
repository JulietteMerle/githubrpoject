//our actions instead of App.js
import React, { useReducer } from 'react';
import axios from 'axios';
import GithubContext from './githubContext';
import GithubReducer from './githubReducer';
import { 
    SEARCH_USERS,
    SET_LOADING,
    CLEAR_USERS,
    GET_USER,
    GET_REPOS
} from '../types';

let githubClientId;
let githubClientSecret;

//check the environment variables = if Node_ENV is not 'production'
if(process.env.Node_ENV !== 'production') {
  githubClientId = process.env.REACT_APP_GITHUB_CLIENT_ID;
  githubClientSecret= process.env.REACT_APP_GITHUB_CLIENT_SECRET;
} else {
  //use global variable
  githubClientId = process.env.GITHUB_CLIENT_ID;
  githubClientSecret= process.env.CLIENT_SECRET;
};

//create initial state
const GithubState = props => {
    const initialState = {
        users: [],
        user: {},
        repos: [],
        loading: false
    }

    const [state, dispatch] = useReducer(GithubReducer, initialState);

    //search github users
    const searchUsers = async text =>{
    setLoading();
    const res = await axios.get(
      `https://api.github.com/search/users?q=${text}&client_id=${
        githubClientId}&client_secret=${
          githubClientSecret}`);
      dispatch({
          type: SEARCH_USERS,
          payload: res.data.items
      });
  };

  //get a single Github user
  const getUser = async (username) => {
    setLoading();
    const res = await axios.get(
      `https://api.github.com/users/${username}?&client_id=${
        githubClientId
      }&client_secret=${githubClientSecret}`);
      dispatch({
          type: GET_USER,
          payload: res.data
      })
  };

  // Get users repos ==> sort create asc (part of github API)
  const getUserRepos = async (username) => {
    setLoading();
    const res = await axios.get(
      `https://api.github.com/users/${username}/repos?per_page=5&sort=created:asc&client_id=${
        githubClientId
      }&client_secret=${githubClientSecret}`);
      dispatch({
        type: GET_REPOS,
        payload: res.data
      })
  };

    //Clear Users
    const clearUsers = () => dispatch({ type: CLEAR_USERS });

    //Set Loading ==> dispatch = need a type 
  const setLoading = () => dispatch({ type: SET_LOADING });

    //need to return the provider = need to wrap the whole app with the provider
    //the provider is gonna take in props = making the values available to the entire app
    return <GithubContext.Provider
        value={{
            users: state.users,
            user: state.user,
            repos: state.repos,
            loading: state.loading,
            searchUsers,
            clearUsers,
            getUser,
            getUserRepos
            }}>

            {props.children}
    </GithubContext.Provider>
}

export default GithubState;

