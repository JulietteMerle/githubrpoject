import { 
    SEARCH_USERS,
    SET_LOADING,
    CLEAR_USERS,
    GET_USER,
    GET_REPOS
} from '../types';


//a reducer is just a function that takes 2 things= a state and an action
//state is immutable == can't reassign it but we can make a copy and change value
export default (state, action) => {
    switch(action.type) {
        case SEARCH_USERS:
            return {
                //return current state==> use spread operator 
                ...state,
                users: action.payload,
                loading: false
            };
        case GET_USER: 
        return {
            ...state,
            user: action.payload,
            loading: false
        }
        case CLEAR_USERS:
            return {
                ...state,
                users: [],
                loading: false
            };
        case GET_REPOS:
            return {
                ...state,
                repos: action.payload,
                loading: false
            };
        case SET_LOADING:
            return {
                ...state,
                loading: true
            };
        default:
            return state;
    }
}